---
title: "Clubs & Associations"
date: 2020-12-20T11:37:00-05:00
draft: false
---

# Clubs and Associations

[Clubs & Associations 2020-2021 PDF](https://www.northernsac.ca/wp-content/uploads/NSS-CLUBS-ASSOCIATIONS-2020-2021.pdf)

The list of clubs and associations will be coming soon.

<ul id="clubs-list">
<li>c</li>
<li>b</li>
<li>a</li>
</ul>
<button onclick="sortUnorderedList('clubs-list', true);">search</button>

<noscript>
    <div class="re-section">
        <i data-feather="alert-circle"></i>Today's date won't be visible without JavaScript enabled.
    </div>
</noscript>
<input type="text" class="w3-bar-item re-input w3-right w3-border" placeholder="Search..." id="clubs-search">
<script>
    function sortUnorderedList(ul, sortDescending) {
        if(typeof ul == "string") ul = document.getElementById(ul);
        var lis = ul.getElementsByTagName("LI");
        var vals = [];
        for(var i = 0, l = lis.length; i < l; i++)
        vals.push(lis[i].innerHTML);
        vals.sort();
        if(sortDescending) vals.reverse();
        for(var i = 0, l = lis.length; i < l; i++) lis[i].innerHTML = vals[i];
    }
    $('#clubs-search')[0].addEventListener("change", function (e) {
        sortUnorderedList('clubs-list', true);
    });
</script>
